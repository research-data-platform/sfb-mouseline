<?php

function sfb_mouseline_view_mouseline_title_callback($mouseline_pid) {

  $title = 'View Mouseline';

  return $title;

}

/**
 * Mouseline details view.
 *
 * @param $mouseline_pid
 *   Database mouseline ID.
 *
 * @return string
 *   HTML code
 */
function sfb_mouseline_view_mouseline($form, &$form_state, $mouseline_pid) {
  // extract the Mouseline id from PID string
  $form_state['mouseline_id'] = Mouseline::extractIDFromPID($mouseline_pid);

  if ($form_state['mouseline_id'] == NULL) {
    drupal_not_found();
    exit();
  }

  // read mouseline data from database
  $mouseline = MouselineRepository::findByid($form_state['mouseline_id']);

  // check if user has permission to view this antibody
  if (!$mouseline->userHasPermissionTo(MouselineAction::READ)) {
    drupal_access_denied();
    exit();
  }

  // output variable with html code

  $output = '';
  $output .= '<div class="row">';

  $output .= '<div class="col-md-6">';

  //
  // mouse line data
  //
  $datasheet_link = '';
  if (user_is_logged_in() || variable_get(SFB_MOUSELINE_CONFIG_DATASHEET_PUBLIC, 0)) {
    $datasheet_link = '
      <a href="' . sfb_mouseline_url(SFB_MOUSELINE_URL_DOWNLOAD_DATASHEET, $mouseline->getId()) . '">
      <img src="' . base_path() . drupal_get_path('module', 'sfb_mouseline') . '/resources/pdf_24px.png" alt="" data-toggle="tooltip" title="Download PDF datasheet" style="float:right;" />
      </a>';
  }
  $output .= '<h3>Basic data' . $datasheet_link . '</h3>';

  $tbl_general_rows = [];
  $tbl_general_rows[] = ['<strong>PID</strong>', $mouseline->getElementPID()];
  $tbl_general_rows[] = [
    '<strong>Research Group</strong>',
    $mouseline->getWorkingGroup()->getName(),
  ];
  $tbl_general_rows[] = [
    '<strong>Sharing Level</strong>',
    $mouseline->getElementSharingLevel(),
  ];

  $output .= theme('table', ['header' => [], 'rows' => $tbl_general_rows]);


  $output .= '<h3>Mouseline data</h3>';

  $tbl_mouseline_rows = [];
  $tbl_mouseline_rows[] = ['<strong>Name</strong>', $mouseline->getName()];
  $tbl_mouseline_rows[] = ['<strong>Type</strong>', $mouseline->getType()];
  if  ($mouseline->getType() == MouselineType::COISOGENIC) {
    $tbl_mouseline_rows[] = [
      '<strong>Strain</strong>',
      MouselineStrainRepository::findById($mouseline->getStrainId())->getName(),
    ];
  }
  if (($mouseline->getType() == MouselineType::INBRED) || ($mouseline->getType() == MouselineType::SUBSTRAIN)){
    $tbl_mouseline_rows[] = [
      '<strong>Strain Abbreviation</strong>',
      MouselineStrainRepository::findById($mouseline->getStrainId())->getAbbreviation(),
    ];
  }
  if ($mouseline->getType() == MouselineType::HYBRID) {
    $tbl_mouseline_rows[] = [
      '<strong>Mother\'s Mouseline</strong>',
      MouselineRepository::findById($mouseline->getMotherLineId())->getName(),
    ];
    $tbl_mouseline_rows[] = [
      '<strong>Father\'s Mouseline</strong>',
      MouselineRepository::findById($mouseline->getFatherLineId())->getName(),
    ];
  }
  if (($mouseline->getType() == MouselineType::MIX_INBRED) ||  ($mouseline->getType() == MouselineType::CONGENIC)) {
    $tbl_mouseline_rows[] = ['<strong>Host Strain Name</strong>', $mouseline->getHostName()];
    $tbl_mouseline_rows[] = ['<strong>Host Strain Abbreviation</strong>', $mouseline->getHostAbbr()];
    $tbl_mouseline_rows[] = ['<strong>Donor Strain Name</strong>', $mouseline->getDonorName()];
    $tbl_mouseline_rows[] = ['<strong>Donor Strain Abbreviation</strong>', $mouseline->getDonorAbbr()];
  }
  $tbl_mouseline_rows[] = [
    '<strong>Origin Laboratory</strong>',
    MouselineLaboratoryRepository::findById($mouseline->getLaboratoryId())->getName(),
  ];
  if ($mouseline->getMutationType() == MouselineMutation::WILDTYPE) {
    $tbl_mouseline_rows[] = ['<strong>Mutation</strong>', "None"];
  }
  else if ($mouseline->getType() !== MouselineType::UNSPECIFIED){
    //$tbl_mouseline_rows[] = ['<strong>Mutation</strong>', $mouseline->getMutationType()];
    // only temporary as long as there is no differentiation between mutation types
    $tbl_mouseline_rows[] = ['<strong>Mutation</strong>', "yes"];
    $tbl_mouseline_rows[] = ['<strong>Affected Gene or Allele</strong>', $mouseline->getMutatedGene()];
    $tbl_mouseline_rows[] = ['<strong>Affected Gene/Allele Details</strong>', $mouseline->getMutatedGeneDetails()];
  }
  $tbl_mouseline_rows[] = ['<strong>Created</strong>', $mouseline->getCreatedDate()];
  $tbl_mouseline_rows[] = ['<strong>Comments</strong>', $mouseline->getComments()];

  $output .= theme('table', ['header' => [], 'rows' => $tbl_mouseline_rows]);

  $output .= '<h3>External Links</h3>';
  $tbl_ext_rows = [];
  $mpdurl = "n/a";
  $ncbiurl = "n/a";
  if (!empty($mouseline->getMpdURL())) {
    $mpdurl = "<a href=" . $mouseline->getMpdURL() . " target=\"_blank\">" . $mouseline->getMpdURL();
  }
  if (!empty($mouseline->getNcbiGeneURL())) {
    $ncbiurl = "<a href=" . $mouseline->getNcbiGeneURL() . " target=\"_blank\">" . $mouseline->getNcbiGeneURL();
  }
  $ilarurl = MouselineLaboratoryRepository::findById($mouseline->getLaboratoryId())
    ->getIlarURL();
  if (empty($ilarurl)) {
    $ilarurl = "n/a";
  }
  else {
    $ilarurl = "<a href=" . $ilarurl . " target=\"_blank\">" . $ilarurl;
  }
  $tbl_ext_rows[] = ['<strong>Mouse Phenome Database URL</strong>', $mpdurl];
  $tbl_ext_rows[] = ['<strong>Laboratory Code ILAR URL</strong>', $ilarurl];
  if (!($mouseline->getMutationType() == MouselineMutation::WILDTYPE)) {
    $tbl_ext_rows[] = ['<strong>NCBI Gene DB URL</strong>', $ncbiurl];
  }

  $output .= theme('table', ['header' => [], 'rows' => $tbl_ext_rows]);

  $output .= '</div>';
  $output .= '<div class="col-md-6">';

  $form['output_infos'] = [
    '#markup' => $output,
  ];

  $output = '';

  //
  // Linked mice
  //

  $output .= '<h3>Linked mice</h3>';

  //Prepare Table header
  $mouse_header = [
    ['data' => t('ID')],
    ['data' => t('Name')],
    ['data' => t('Sex')],
    ['data' => t('Date of Birth')],
    ['data' => t('Date of Death')],
    //array('data' => t('Working Group')),
    ['data' => t('Comments')],
    ['data' => t('Created')],
    ['data' => t('Edit')],
  ];

  //Get all mice associated with this mouseline
  $mice = MouselineMouseRepository::findByMouselineId($form_state['mouseline_id']);

  // create table rows
  $mouse_rows = [];

  // each table row is a table row
  foreach ($mice as $mouse) {
    if ($mouseline->userHasPermissionTo(MouselineAction::WRITE)) {
      $mouse_rows[] = [
        $mouse->getId(),
        '"' . $mouse->getName() . '"',
        $mouse->getSex(),
        $mouse->getDateOfBirth(),
        $mouse->getDateOfDeath(),
        //$mouse->getWorkingGroup(),
        $mouse->getComments(),
        $mouse->getCreatedDate(),
        '<a href="' . sfb_mouseline_url(SFB_MOUSELINE_URL_EDIT_MOUSE,
          $mouseline->getElementPID(),
          $mouse->getElementPID()) . '"><span class="glyphicon glyphicon-cog"></span></a>',
      ];
    }
    else {
      $mouse_rows[] = [
        $mouse->getId(),
        '"' . $mouse->getName() . '"',
        $mouse->getSex(),
        $mouse->getDateOfBirth(),
        $mouse->getDateOfDeath(),
        //$mouse->getWorkingGroup(),
        $mouse->getComments(),
        $mouse->getCreatedDate(),
        '',
      ];
    }
  }

  $output .= theme('table', ['header' => $mouse_header, 'rows' => $mouse_rows]);
  if ($mouseline->userHasPermissionTo(MouselineAction::WRITE)) {
    $output .= '<a href="' . sfb_mouseline_url(SFB_MOUSELINE_URL_ADD_MOUSE,
        $mouseline->getElementPID()) . '" class="btn btn-primary" >
    <span class="glyphicon glyphicon-plus"></span> Mice</a>';
  }
  $form['output_mice'] = [
    '#markup' => $output,
  ];
  $output = '';

  $output .= '<h3>Linked publications</h3><hr />';
  $publications = $mouseline->getLinkedPublications();

  //TODO: das ist eine temporäre lösung:
  if (count($publications['table']['rows']) == 0) {
    $output .= 'There are no linked publications.';
  }
  else {
    foreach ($publications['table']['rows'] as $publication) {
      /**
       * Make publication title a link, if URL is available (requires literature module version >=1.1)
       */
      $output .= '<p>';
      if (isset($publication[8])) {
        $output .= l('<span style="font-size: 0.85em; font-weight: bold;">' . $publication[0] . '</span><br />',
          $publication[8], ['html' => TRUE]); //Title
      }
      else {
        $output .= '<span style="font-size: 0.85em; font-weight: bold;">' . $publication[0] . '</span><br />'; //Title
      }
      $output .= '<span style="font-size: 0.8em">' . $publication[1] . '</span><br />'; // Authors
      $output .= '<span style="font-size: 0.8em">' . $publication[6] . '</span> '; // Journal
      $output .= '<span style="font-size: 0.8em">' . $publication[4] . '</span> : '; // publication year
      $output .= '<span style="font-size: 0.8em"><a href="https://dx.doi.org/' . $publication[3] . '">' . $publication[3] . '</a></span>'; // doi
      $output .= '</p>';
    }
  }

  $output .= '</div>';
  $output .= '</div><br>';

  $output .= '<a href="' . sfb_mouseline_url(SFB_MOUSELINE_URL_PAGE_DEFAULT) . '" class="btn btn-default">Back</a> ';
  if ($mouseline->userHasPermissionTo(MouselineAction::WRITE)) {
    $output .= '<a href="' . sfb_mouseline_url(SFB_MOUSELINE_URL_EDIT_MOUSELINE,
        $mouseline->getElementPID()) . '" class="btn btn-primary">Edit Mouseline</a> ';
  }
  $form['text'] = [
    '#markup' => $output,
  ];

  return $form;
}

/*
 * ajax
 */
function sfb_mouseline_add_data_save($form, &$form_state) {
  $rdp_linking = RDPLinking::getLinkingByName('sfb_antibody', 'antibody_antibody2');
  $rdp_linking->processSubmittedForm('mouselines_mouseline', $form_state['mouseline_id'],
    $form_state['values']['antibody_edit']);

  $rdp_linking = RDPLinking::getLinkingByName('sfb_antibody',
    'antibody_antibody2');
  $table = $rdp_linking->getViewData('mouselines_mouseline',
    $form_state['mouseline_id']);
  $form['ab_items'] = [
    '#markup' => '<div id="antibody-div">' . $table,
  ];

  $form['antibody_edit'] = $rdp_linking->getEditForm('mouselines_mouseline',
    $form_state['mouseline_id']);

  $commands[] = ajax_command_replace('#antibody-div',
    render($form['ab_items']));

  return ['#type' => 'ajax', '#commands' => $commands];
}
