<?php
function sfb_mouseline_help($form, &$form_state)
{

  $page_prefix = '<div><table class="table"><tbody><tr>';
  $introduction = '<td><h3>Introduction: What is a Mouseline Catalogue?</h3></td><th>
                </th></tr>
                <tr><td><img src="'.base_path().drupal_get_path('module', 'sfb_mouseline').'/resources/Help1_Overview.png" alt="" data-toggle="tooltip" title="Overview page of the Mouse Line catalogue" style="float:left;" /></td>
                <td>
                    The goal of this Module is to give you as a researcher the possibility to store and share your mouse related research data
                    on a standardised and persistent plattform. This is achieved by using the PID handling system offered by the EPIC Consortium and
                    generating standardised names for your mouselines. This module uses the nomenclature implemented by the International Committee on Standardized Genetic Nomenclature of Mice.<br>
                    In this process you have the control over who you want to share your data with: your research group, all users of the Research Data Platform or the open public.<br>
                    The data sets can be linked to your publications to give a complete and reproducible overview over your findings.<br>
                </td></tr>';

  $create_new = '<td><h3>How to enter a new mouse line</h3>
                </td>
                <tr><td>When you want to create a new data set for a mouse line you have to select a breeding type first. A short description of the selected type will be shown below you selection<br>
                In the following form you can enter all relevant information for you mouse line. Most fields are optional and can be left empty. Exeptions are the origin strain\'s name, your research group and
                the sharing level of this data set. The latter defines which permissions are needed to view this data.<br>
                Although the rest of the fields is optional it is recommended to fill them as well as it may reduce unnecessary effort concerning data reproduction in a later phase of your work. <br>
                </td>
                <td><img src="'.base_path().drupal_get_path('module', 'sfb_mouseline').'/resources/Help2_New.png" alt="" data-toggle="tooltip" title="When registering a new mouse line, the origin strain\'s name, your research group and the dataset\'s visibility are mandatory fields." style="float:left;" />

                </td></tr>';

  $paper_view = '<td><h3>How to link your mouse lines to your scientific publications</h3></td><th>
                </td>
                <tr><td><img src="'.base_path().drupal_get_path('module', 'sfb_mouseline').'/resources/Help3_Link.png" alt="" data-toggle="tooltip" title="You can link your mouse lines by adding their IDs to this text field." style="float:left;" />
                <td>
                    As mentioned before, one of the mouseline catalogue\'s (and the Research Data Platform in general) goal is to make scientific papers more reproducible. For this reason you can link your
                    mouse line data sets to the corresponding publication.<br>
                    To do this you can select you publication which has to be entered into the \'Publications\' module beforehand and edit it. Scroll down and
                    you will find a section where you can link mouse lines with this publication. Simply enter the first letters of the desired mouse line and selectable options will appear. You can add as many
                    mouse lines in this process as you like. Any mouse line can be removed by klicking the \'x\' next to its tag in the text field.<Br>Finally save your changes and check the result in the publication\'s overview.<br>
                </td></tr></tbody></table>';

  $links = '<h3>Helpful Links</h3>
Mouse Phenome Database: <a href="http://phenome.jax.org/strains" target=\"_blank\">http://phenome.jax.org/strains</a><br>
Nomenclature for mice: <a href="http://www.informatics.jax.org/mgihome/nomen/strains.shtml" target="_blank">http://www.informatics.jax.org/mgihome/nomen/strains.shtml</a><br>
Short Guide on naming transgenic mouse lines: <a href="https://www.jax.org/jax-mice-and-services/customer-support/technical-support/genetics-and-nomenclature" target="_blank">https://www.jax.org/jax-mice-and-services/customer-support/technical-support/genetics-and-nomenclature</a><br>
ILAR Codes for laboratories: <a href="http://dels.nas.edu/global/ilar/Lab-Codes" target="_blank">http://dels.nas.edu/global/ilar/Lab-Codes</a><br>
NCBI Gene Database: <a href="http://www.ncbi.nlm.nih.gov/gene" target="_blank">http://www.ncbi.nlm.nih.gov/gene</a><br>
';

  $page_suffix = "</div>";
  $output = $page_prefix.$introduction.$create_new.$paper_view.$links.$page_suffix;
  $output .= '<a class="btn btn-default" href="'. sfb_mouseline_url(SFB_MOUSELINE_URL_PAGE_DEFAULT) .'">Back to Main Page</a>';

  $form['text'] = Array(
      '#markup' => $output,
  );
  return $form;
}
