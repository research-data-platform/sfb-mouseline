<?php

/**
 * Download page
 *
 * @param $option string
 *    Download handler options
 */
function sfb_mouseline_download($option) {

  // if option is not set, then return drupal not found page
  if($option == NULL) {
    drupal_not_found();
    return;
  }

  if ($option == 'datasheet' && !empty(arg(3))) {
    return sfb_mouseline_download_sheet(arg(3));

  } else {
    drupal_not_found();
    exit();
  }

}

/**
 * @param $mouseline_id int
 */
function sfb_mouseline_download_sheet($mouseline_id) {

  // get an mouseline
  $mouseline = MouselineRepository::findById($mouseline_id);

  if($mouseline->isEmpty()) {
    drupal_not_found();
    exit();
  }

  // check user permission
  if(!$mouseline->userHasPermissionTo(MouselineAction::READ)) {
    drupal_not_found();
    exit();
  }

  // load pdf library
  module_load_include('php', 'sfb_commons', 'lib/tcpdf_min_6.2.12/tcpdf');
  module_load_include('php', 'sfb_commons', 'lib/fpdi_1.6.1/fpdi');

  class PDF extends FPDI
  {
    /**
     * "Remembers" the template id of the imported page
     */
    var $_tplIdx;

    var $templateFileName = 'template_mouseline.pdf';

    /**
     * Draw an imported PDF logo on every page
     */
    function Header()
    {

      if (is_null($this->_tplIdx)) {
        $template_file = $_SERVER['DOCUMENT_ROOT'] . base_path().drupal_get_path('module', 'sfb_mouseline').'/resources/'.$this->templateFileName;
        $this->setSourceFile($template_file);
        $this->_tplIdx = $this->importPage(1);
      }
      $tplIdx = $this->importPage(1, '/MediaBox');
      $size = $this->useTemplate($tplIdx, 0, 0, 0, 0, true);
    }


    function Footer()
    {
      // position at 15 mm from bottom
      $this->SetY(-15);
      // set font
      $this->SetFont('helvetica', 'I', 8);
      // page number
      $this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }

    function setTemplateFileName($filename) {
      $this->templateFileName = $filename;
    }
  }

  // initiate PDF object
  $pdf = new PDF();

  $pdf->SetFont('sourcesanspro', '', 10, '', false);
  $pdf->SetMargins(PDF_MARGIN_LEFT, 40, PDF_MARGIN_RIGHT);
  $pdf->SetAutoPageBreak(true, 40);
  $pdf->setFontSubsetting(false);

  _sfb_mouseline_download_sheet_add_mouseline_data($pdf, $mouseline);


  // prepare drupal for pdf output
  drupal_add_http_header('Access-Control-Allow-Origin', "*");
  drupal_add_http_header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  header('Content-Disposition: attachment;filename="mouseline-datasheet-'.$mouseline_id.'.pdf"');

  // print pdf file
  $pdf->Output();
  exit();

}//

/**
 *
 * @param $pdf
 * @param $mouseline \Mouseline
 */
function _sfb_mouseline_download_sheet_add_mouseline_data(&$pdf, $mouseline) {
  $pdf->AddPage();

  // add page header: mouseline pid and epic pid
  _sfb_mouseline_download_sheet_add_mouseline_header($pdf, $mouseline);

  // add basic mouseline data
  _sfb_mouseline_download_sheet_mouseline_basic_data($pdf, $mouseline);

}

/**
 * @param $pdf
 * @param $mouseline
 */
function _sfb_mouseline_download_sheet_add_mouseline_header(&$pdf, $mouseline) {
  $html  = '<span style="font-size: 1.8em;font-family: sourcesansprolight">'.$mouseline->getElementPID().'</span><br />';
  //$html .= '<span style="font-size: 0.7em">'.$mouseline->getElementPID().'</span><br />';
  $pdf->writeHTML($html, true, false, true, false, '');
}

/**
 * @param $pdf \TCPDF
 * @param $mouseline \Mouseline
 */
function _sfb_mouseline_download_sheet_mouseline_basic_data(&$pdf, $mouseline) {

  $html = '
    <table cellpadding="3">
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Name</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getName().'</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Type</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getType().'</td></tr>';
  if(($mouseline->getType() == MouselineType::INBRED) || ($mouseline->getType() == MouselineType::SUBSTRAIN)){
    $html .= '<tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Strain</td><td style="border: 0.5px solid #e4dfcd;">'.MouselineStrainRepository::findById($mouseline->getStrainId())->getName().'</td></tr>';

  } else if ($mouseline->getType() == MouselineType::HYBRID){
    $html .= '<tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Mother\'s Line</td><td style="border: 0.5px solid #e4dfcd;">'.MouselineRepository::findById($mouseline->getMotherLineId())->getName().'</td></tr>
              <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Father\'s Line</td><td style="border: 0.5px solid #e4dfcd;">'.MouselineRepository::findById($mouseline->getFatherLineId())->getName().'</td></tr>';
  } else if ($mouseline->getType() == MouselineType::MIX_INBRED){
      $html .= '<tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Host Strain</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getHostName().'</td></tr>
        <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Donor Strain</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getDonorName().'</td></tr>';
  }

  $html .= '<tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Mutation</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getMutationType().'</td></tr>';
  if ($mouseline->getType() == MouselineType::MIX_INBRED){
    $html .= '<tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Affected Gene</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getMutatedGeneSymbol().'</td></tr>
    <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Affected Gene Details</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getMutatedGeneDetails().'</td></tr>';
  }
  $html .= '<tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Origin Laboratory</td><td style="border: 0.5px solid #e4dfcd;">'.MouselineLaboratoryRepository::findById($mouseline->getLaboratoryId())->getName().'</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Comments</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getComments().'</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Created date</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getCreatedDate().'</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Last modified</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getLastModified().'</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Research Group</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getWorkingGroup()->getName().'</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;">Sharing level</td><td style="border: 0.5px solid #e4dfcd;">'.$mouseline->getElementSharingLevel().'</td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
      <tr><td style="border: 0.5px solid #e4dfcd; font-family: sourcesansprolight;"></td><td style="border: 0.5px solid #e4dfcd;"></td></tr>
    </table>';

  $pdf->writeHTML($html, true, false, true, false, '');

}
