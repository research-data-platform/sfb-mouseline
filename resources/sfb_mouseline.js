/**
 * Created by Paul_Roman on 13.01.2017.
 */
/**
 * Function used by select2 library for tags formatting.
 *
 * @param repo
 * @returns {*}
 */
function formatTags(repo) {
    if (repo.loading) return repo.text;

    var markup = "" +
        "<div class='select2-result-repository clearfix'>" +
        repo.name +
        "</div>";

    return markup;
}

function formatMouselines (repo) {
    if (repo.loading) return repo.text;

    var markup = "" +
        "<div class='select2-result-repository clearfix'>" +
        "<div class='media'>" +
        "<div class='media-left'>" +
        "<img src='https://sfb1002.med.uni-goettingen.de/development/sites/all/modules/sfb-antibody/icon32_" + repo.type + ".png'>" +
        "</div>" +
        "<div class='media-body'><h4 class='media-heading'>" + repo.pid + (repo.name == null ? "" : " - " + repo.name) + "</h4>" +
            //(repo.antibody_alt_name==null?"":"<p><small>" + repo.alternative_name + "</small></p>") +
        "<dl class='dl-horizontal'>" +
        "<dt><small>Type:</small></dt><dd><small>" + repo.type + "</small></dd>" +
            //"<dt><small>Antigen Symbol:</small></dt><dd><small>" + (repo.antigen_symbol==null?"-":repo.antigen_symbol) + "</small></dd>" +
            //"<dt><small>Antibody Registry ID:</small></dt><dd><small>" + (repo.antibody_registry_id==null?"-":repo.antibody_registry_id) + "</small></dd>" +
            //"<dt><small>Catalog No:</small></dt><dd><small>" + (repo.catalog_no==null?"-":repo.catalog_no) + "</small></dd>" +
        "</dl>" +
        "</div>" +
        "</div>" +
        "</div>";

    return markup;
}

function formatMouse (repo) {
    if (repo.loading) return repo.text;

    var markup = "" +
        "<div class='select2-result-repository clearfix'>" +
        "<div class='media'>" +
        "<div class='media-left'>" +
        "<img src='https://sfb1002.med.uni-goettingen.de/development/sites/all/modules/sfb-antibody/icon32_" + repo.type + ".png'>" +
        "</div>" +
        "<div class='media-body'><h4 class='media-heading'>" + repo.pid + (repo.name == null ? "" : " - " + repo.name) + "</h4>" +
        //(repo.antibody_alt_name==null?"":"<p><small>" + repo.alternative_name + "</small></p>") +
        "<dl class='dl-horizontal'>" +
        //"<dt><small>Type:</small></dt><dd><small>" + repo.type + "</small></dd>" +
        //"<dt><small>Antigen Symbol:</small></dt><dd><small>" + (repo.antigen_symbol==null?"-":repo.antigen_symbol) + "</small></dd>" +
        //"<dt><small>Antibody Registry ID:</small></dt><dd><small>" + (repo.antibody_registry_id==null?"-":repo.antibody_registry_id) + "</small></dd>" +
        //"<dt><small>Catalog No:</small></dt><dd><small>" + (repo.catalog_no==null?"-":repo.catalog_no) + "</small></dd>" +
        "</dl>" +
        "</div>" +
        "</div>" +
        "</div>";

    return markup;
}

/**
 * Function used by select2 library for adding tags to the select field
 *
 * @param repo
 * @returns {*}
 */
function formatTagsSelection(repo) {
    return repo.name || repo.text;
}

function formatMouselinesSelection (repo) {
    return repo.pid || repo.text;
}

function formatMouseSelection (repo) {
    return repo.pid || repo.text;
}

function inittags(scriptid, selector) {
    var jsonpath = jQuery(selector).attr('data-source');

    var bloodhound = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: jsonpath,
            filter: function(list) {
                return jQuery.map(list, function(entry) {
                    return { name: entry };
                });
            },
            ttl: 1,
            ttl_ms: 1,
            cache: false,
        },
    });

    bloodhound.initialize();

    jQuery(selector).tagsinput({
        trimValue: true,
        allowDuplicates: false,
        typeaheadjs: {
            name: scriptid,
            displayKey: 'name',
            valueKey: 'name',
            source: bloodhound.ttAdapter()
        }
    });


    jQuery(selector).tagsinput('input').blur(function() {
        jQuery(selector).tagsinput('add', jQuery(this).val());
        jQuery(this).val('');
    });
}

function gomoho(scriptid, selector, mohos) {
    var elem = jQuery(selector);

    jQuery.each(mohos, function(i,v) { elem.tagsinput('add', v); });
}

function initselect(scriptid, selector) {
    var jsonpath = jQuery(selector).attr('data-source');

    var bloodhound = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: jsonpath,
            filter: function(list) {
                return jQuery.map(list, function(entry) {
                    return { name: entry };
                });
            },
            ttl: 1,
            ttl_ms: 1,
            cache: false,
        },
    });

    bloodhound.initialize();

    jQuery(selector).typeahead({
            minLength: 0,
            highlight: true
        },
        {
            name: scriptid,
            displayKey: 'name',
            valueKey: 'name',
            source: bloodhound.ttAdapter()
        }
    );
}