<?php

/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 02.03.2017
 * Time: 14:10
 */
class MouselineMutation
{
  const TARGETED_MUTATION = "targeted mutation";

  const OTHER_MUTATION = "mutation";

  const WILDTYPE = "wildtype";

  //TODO MORE TO COME

  public static function getAllMutations(){
    return Array(
      MouselineMutation::TARGETED_MUTATION,
      MouselineMutation::OTHER_MUTATION,
      MouselineMutation::WILDTYPE,
    );
  }

  public static function buildMutationName($mouseline, $name, $mutation, $gene, $lab){
      /**
       * details about the mutation on the specific gene
       *
       * @var string
       */
      //$details = 'test';

    if($mutation == MouselineMutation::WILDTYPE){
      return $name;
    }
    //case that lab abbreviation needs to be set at the end of the name
    else {
        /*$array = explode("/",$name);
        $mutname = $array[0];
        if(!empty($array[1]))
            $lab = $array[1];*/

        if ($mutation == MouselineMutation::OTHER_MUTATION) {
            if (empty($gene))
                $gene = '{gene}';
            $details = $mouseline->getMutatedGeneDetails();
            //  drupal_set_message('Gen Details: '.$details);

            // gene name and gene details in italics
            if (empty($lab))
                return $name . '-<i>'. $gene . '<sup>' . $details .'</sup></i>';
            else
                return $name . '-<i>' . $gene . '<sup>' . $details .'</sup></i>' .'/' .$lab;

        }
    }
  }
}
