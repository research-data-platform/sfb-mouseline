<?php

/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 05.04.2017
 * Time: 15:08
 */
class MouselineLaboratoryRepository
{
  /* ------------------------------ DB functionality ----------------------------- */ //TODO: evtl. auslagern in eigenes File

  /**
   * laboratory db-table name
   *
   * @var string
   */
  static $tableName = 'mouselines_laboratory';

  /**
   * defines the table fields
   *
   * @var array
   */
  static $databaseFields = array(
    'id',
    'name',
    'abbreviation',
    'ilar_url',
  );

  /**
   * Stores lab data into database.
   *
   * @param MouselineLaboratory $laboratory
   * @return \DatabaseStatementInterface|int
   */
  public static function save ($laboratory){
    if($laboratory->isEmpty()) {
      $nid = db_insert(self::$tableName)
        ->fields(array(
          'name' => $laboratory->getName(),
          'abbreviation' => $laboratory->getAbbreviation(),
          'ilar_url' => $laboratory->getIlarURL(),
        ))
        ->execute();
      return $nid;
    } else {
      $num_updated = db_update(self::$tableName)
        ->fields(array(
          'name' => $laboratory->getName(),
          'abbreviation' => $laboratory->getAbbreviation(),
          'ilar_url' => $laboratory->getIlarURL(),
        ))
        ->condition('id',$laboratory->getId(), '=')
        ->execute();
      return -1;
    }
  }

  /**
   * Looks up lab with given id in the data base
   *
   * @param $id
   * @return MouselineLaboratory
   */
  public static function findById($id)
  {
    $result = db_select(self::$tableName, 'l')
      ->condition('id', $id, '=')
      ->fields('l', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToLaboratory($result);
  }

  /**
   * Looks up lab with given name in the data base
   *
   * @param $name
   * @return MouselineLaboratory
   */
  public static function findByName($name)
  {
    $result = db_select(self::$tableName, 'l')
      ->condition('name', $name, '=')
      ->fields('l', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToLaboratory($result);
  }

  /**
   * after db query is done, the results are translated into MouselineLaboratory object
   *
   * @param $result
   * @return MouselineLaboratory
   */
  public static function databaseResultsToLaboratory($result){
    $laboratory = new MouselineLaboratory();

    if(empty($result)) {
      return $laboratory;
    }
    // set variables
    $laboratory->setId($result->id);
    $laboratory->setName($result->name);
    $laboratory->setAbbreviation($result->abbreviation);
    $laboratory->setIlarURL($result->ilar_url);
    return $laboratory;
  }

  /**
   * Executes search query on Laboratory database with given condition.
   *
   * @param $condition Database condition
   * @return \MouselineLaboratory[]
   */
  public static function findBy($condition) {
    // sharing level condition

    $results = db_select(self::$tableName, 'm')
      ->condition($condition)
      ->fields('m', self::$databaseFields)
      ->execute();

    return self::databaseResultsToLaboratories($results);
  }

  /**
   * Reads lab database results and create an array with lab objects.
   *
   * @param $results
   * @return \MouselineLaboratory[]
   */
  public static function databaseResultsToLaboratories($results) {
    $labs = array();
    foreach($results as $result)
      $labs[] = self::databaseResultsToLaboratory($result);

    return $labs;
  }
}