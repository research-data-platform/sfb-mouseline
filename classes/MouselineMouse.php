<?php

/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 06.01.2017
 * Time: 15:26
 */
class MouselineMouse {

  /* ------------------------------ Attributes ----------------------------- */

  const EMPTY_MOUSE_ID = -1;

  /**
   * Level of visibility for the individual mouse, values:
   * 10 = private, currently not used
   * 20 = (research) group wide
   * 30 = site/rdp wide
   * 90 = public
   * uses the sharing levels defined in mouseline
   *
   * @see MouselineSharingLevel
   * @var int
   */
  private $sharingLevel;

  /**
   * @var int
   */
  private $id = MouselineMouse::EMPTY_MOUSE_ID;


  /**
   * name given by researcher to identify his/her mouse for further actions,
   * not standardised
   *
   * @var string
   */
  private $name;

  /**
   * the mouse line to which this mouse belongs
   *
   * @var int
   */
  private $mouselineId;

  /**
   * the mouse's sex
   *
   * @var string
   */
  private $sex;

  /**
   * the mouse's date of birth
   * format: yyy-mm-dd
   *
   * @var string
   */
  private $date_of_birth;

  /**
   * the mouse's date of death
   * format: yyy-mm-dd
   *
   * @var string
   */
  private $date_of_death;

  /**
   * the working/research group working with this mouse
   *
   * @var string
   */
  private $working_group;

  /**
   * custom mouse comments
   *
   * @var string
   */
  private $comments;

  /**
   * the date the mouse data was created
   * format: yyyy-mm-dd
   *
   * @var string
   */
  private $created_date;

  /**
   * the data of the latest change to this data set
   * format: yyyy-mm-dd
   *
   * @var string
   */
  private $last_change_date;

  /**
   * the mouse's working group's id
   *
   * @var int
   */
  private $workingGroupId;


  /* ------------------------------ Setters & Getters ----------------------------- */


  /**
   * @return int
   */
  public function getSharingLevel() {
    return $this->sharingLevel;
  }

  /**
   * @param int $sharingLevel
   */
  public function setSharingLevel($sharingLevel) {
    $this->sharingLevel = $sharingLevel;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }


  /**
   * @return int $mouselineid
   */
  public function getMouselineId() {
    return $this->mouselineId;
  }

  /**
   * @param int $mouselineId
   */
  public function setMouselineId($mouselineId) {
    $this->mouselineId = $mouselineId;
  }

  /**
   * @return string
   */
  public function getSex() {
    return $this->sex;
  }

  /**
   * @param string $sex
   */
  public function setSex($sex) {
    $this->sex = $sex;
  }

  /**
   * @return string
   */
  public function getDateOfBirth() {
    return $this->date_of_birth;
  }

  /**
   * @param string $date_of_birth
   */
  public function setDateOfBirth($date_of_birth) {
    $this->date_of_birth = $date_of_birth;
  }

  /**
   * @return string
   */
  public function getDateOfDeath() {
    return $this->date_of_death;
  }

  /**
   * @param string $date_of_death
   */
  public function setDateOfDeath($date_of_death) {
    $this->date_of_death = $date_of_death;
  }

  /**
   * @return WorkingGroup
   */
  public function getWorkingGroup() {
    return WorkingGroupRepository::findById($this->workingGroupId);
  }


  /**
   * @param string $working_group
   */
  public function setWorkingGroup($working_group) {
    $this->working_group = $working_group;
  }

  /**
   * @return string
   */
  public function getComments() {
    return $this->comments;
  }

  /**
   * @param string $comments
   */
  public function setComments($comments) {
    $this->comments = $comments;
  }

  /**
   * @return string
   */
  public function getCreatedDate() {
    return $this->created_date;
  }

  /**
   * @param string $created_date
   */
  public function setCreatedDate($created_date) {
    $this->created_date = $created_date;
  }

  /**
   * @return string
   */
  public function getLastChangeDate() {
    return $this->last_change_date;
  }

  /**
   * @param string $last_change_date
   */
  public function setLastChangeDate($last_change_date) {
    $this->last_change_date = $last_change_date;
  }

  /**
   * @return int
   */
  public function getWorkingGroupId() {
    return $this->workingGroupId;
  }

  /**
   * @param int $workingGroupId
   */
  public function setWorkingGroupId($workingGroupId) {
    $this->workingGroupId = $workingGroupId;
  }

  /* ------------------------------ Form fields ----------------------------- */
  /**
   * returns the form field for the name of the mouse
   *
   * @return array
   */
  public function getFormFieldName($required = FALSE, $prefix = '', $suffix = '', $title = 'visible') {
    return [
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $this->name,
      '#description' => 'Here you can add an identification for your own documentation.',
      '#attributes' => ['placeholder' => t('e.g.') . ': #1, ABC, etc'],
      '#maxlength' => 100,
      '#required' => $required,
      '#prefix' => $prefix,
      '#suffix' => $suffix,
      '#title_display' => $title,
      '#size' => 20,
    ];
  }

  /**
   * returns a dropdown to select the mouse's sex
   *
   * @return array
   */
  public function getFormFieldSex($required = FALSE, $prefix = '', $suffix = '', $title = 'visible') {
    return [
      '#type' => 'select',
      '#title' => t('Sex'),
      '#default_value' => $this->sex,
      '#description' => 'Select the mouse\'s sex.',
      '#options' => [
        'Female' => 'Female',
        'Male' => 'Male',
        'Unknown' => 'Unknown',
      ],
      '#required' => $required,
      '#prefix' => $prefix,
      '#suffix' => $suffix,
      '#title_display' => $title,

    ];
  }

  /**
   * adds the form field to enter the date of birth
   *
   * @return array
   */
  public function getFormFieldDateOfBirth($required = FALSE, $prefix = '', $suffix = '', $title = 'visible') {
    return [
      '#type' => 'textfield',
      '#title' => t('Date of Birth'),
      '#default_value' => $this->date_of_birth,
      '#description' => 'Insert the day the mouse was born.',
      '#attributes' => ['placeholder' => t('e.g.') . ': 2017-01-01'],
      '#maxlength' => 100,
      '#required' => $required,
      '#prefix' => $prefix,
      '#suffix' => $suffix,
      '#size' => 15,
      '#title_display' => $title,

    ];
  }

  /**
   * adds the form field to enter the date of death
   *
   * @return array
   */
  public function getFormFieldDateOfDeath($required = FALSE, $prefix = '', $suffix = '', $title = 'visible') {
    return [
      '#type' => 'textfield',
      '#title' => t('Date of Death'),
      '#default_value' => $this->date_of_death,
      '#description' => 'Insert the day the mouse died.',
      '#attributes' => ['placeholder' => t('e.g.') . ': 2017-02-02'],
      '#maxlength' => 100,
      '#required' => $required,
      '#prefix' => $prefix,
      '#suffix' => $suffix,
      '#size' => 15,
      '#title_display' => $title,

    ];
  }

  /**
   * adds a text field for comments
   *
   * @return array
   */
  public function getFormFieldComments($required = FALSE, $prefix = '', $suffix = '', $title = 'visible') {
    return [
      '#type' => 'textfield',
      '#title' => t('Comments'),
      '#default_value' => $this->comments,
      '#attributes' => ['placeholder' => t('e.g.') . ' important information'],
      '#description' => 'Here you can insert information you want others to know.',
      '#maxlength' => 100,
      '#required' => $required,
      '#prefix' => $prefix,
      '#suffix' => $suffix,
      '#size' => 20,
      '#title_display' => $title,

    ];
  }

  public function getFormFieldNumber() {
    return [
      '#type' => 'textfield',
      '#title' => t('Add number of field sets for mice'),
      '#default_value' => $this->comments,
      '#description' => 'Insert a number of field sets you want to add to this page.',
      '#maxlength' => 100,
      '#size' => 10,
    ];
  }

  //Eigentlich nicht nötig, da im Moment eine Mauslinie eh nur einer AG gehören kann. Daher müssen alle
  //entsprechenden Mäuse auch zur gleichen AG gehören. Evtl für spätere Nutzung und Funktionen relevant
  /**
   * adds a select field to connect this mouse to a working group. available working groups are fetched by looking
   * up the user's working groups. currently unused.
   *
   * @return array
   */
  public function getFormFieldWorkingGroup() {
    global $user;

    $userc = UsersRepository::findByUid($user->uid);
    $workingGroups = $userc->getUserWorkingGroups();

    $workingGroupsSelect = [];
    foreach ($workingGroups as $workingGroup) {
      $workingGroupsSelect[$workingGroup->getId()] = $workingGroup->getName();
    }

    $default_value = $this->workingGroupId;

    //TODO: fall einbauen, dass der benutzer zu keiner arbeitsgruppe gehört

    return [
      '#type' => 'select',
      '#title' => t('Research Group'),
      '#required' => TRUE,
      '#options' => $workingGroupsSelect,
      '#default_value' => $default_value,
      '#description' => t('Select research group owning this mouse.'),
      '#prefix' => '<div class="col-md-2">',
      '#suffix' => '</div>',
    ];
  }

  /**
   * Drop down to select the sharing level / visibility of the data set
   *
   * @return array
   */
  public function getFormFieldSharingLevel($prefix = '', $suffix = '', $title = 'visible') {
    $allSharingLevels = MouselineSharingLevel::getAsArray();

    if (empty($this->sharingLevel)) {
      $this->sharingLevel = variable_get(SFB_MOUSELINE_CONFIG_DEFAULT_SHARING_LEVEL,
        MouselineSharingLevel::GROUP_LEVEL);
    }

    return [
      '#type' => 'select',
      '#title' => t('Sharing level'),
      '#description' => 'Change the visibility (sharing) level of this mouse.',

      '#default_value' => $this->sharingLevel,
      '#required' => TRUE,
      '#options' => $allSharingLevels,
      '#prefix' => $prefix,
      '#suffix' => $suffix,
      '#title_display' => $title,

    ];
  }

  /* ------------------------------ Fieldviews ----------------------------- */
  /**
   * inserts the working group's icon, embedded in HTML code
   *
   * @return string
   */
  public function getElementWorkingGroupIcon() {
    $workingGroup = $this->getWorkingGroup();
    if ($workingGroup->isEmpty()) {
      return '<img src="' . base_path() . drupal_get_path('module',
          'sfb_commons') . '/resources/ag_unknown20x20.png" alt="" data-toggle="tooltip" title="Unknown Research Group" />';
    }

    //TODO: ausgeben, falls datei fehlt
    $wgAbbr = $workingGroup->getShortName();
    $wgName = $workingGroup->getName();

    return '<img src="' . base_path() . drupal_get_path('module',
        'sfb_commons') . '/resources/' . $wgAbbr . '20x20.png" alt="" data-toggle="tooltip" title="' . $wgName . '" />';
  }

  public function getElementId() {
    return str_pad($this->id, 4, '0', STR_PAD_LEFT);
  }

  /**
   * takes the mouse line's id and returns the PID with pattern 'mouseline-{00ID}'
   *
   * @return mixed
   */
  public function getElementPID() {

    $pid_id = $this->id;
    // if padding > 0, then pad a id string to given length
    if (variable_get(SFB_MOUSELINE_CONFIG_PID_ID_PADDING, 4) > 0) {
      $pid_id = str_pad($pid_id, variable_get(SFB_MOUSELINE_CONFIG_PID_ID_PADDING, 4), 0, STR_PAD_LEFT);
    }

    $pattern = variable_get(SFB_MOUSELINE_CONFIG_PID_PATTERN, '{type}-{pid}');

    $replaced = preg_replace(['/{type}/', '/{pid}/'], ['mouse', $pid_id], $pattern);

    return $replaced;
  }

  /**
   * saves the mouse , calls database function
   *
   * @return DatabaseStatementInterface|int
   */
  public function save() {
    $nid = MouselineMouseRepository::save($this);
    return $nid;
  }

  /**
   * Checks whether given mouse is new or already in DB
   *
   * @return bool
   */
  public function isEmpty() {
    if ($this->id == MouselineMouse::EMPTY_MOUSE_ID) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * checks whether the user is allowes to {MouselineAction} given mouse
   *
   * @param MouselineAction $action
   *
   * @return bool
   * @see MouselineShareLevel
   * @see MouselineAction
   *
   */
  public function userHasPermissionTo($action) {
    global $user;
    $userc = UsersRepository::findByUid($user->uid);

    if ($action == MouselineAction::READ) {

      if ($this->getSharingLevel() == MouselineSharingLevel::PUBLIC_LEVEL) {
        return TRUE;
      }

      if (user_is_anonymous()) {
        return FALSE;
      }

      if ($this->getSharingLevel() == MouselineSharingLevel::SITE_LEVEL) {
        return TRUE;
      }

      $userc = UsersRepository::findByUid($user->uid);

      if ($userc->isEmpty()) {
        return FALSE;
      }

      if ($this->getSharingLevel() == MouselineSharingLevel::GROUP_LEVEL) {
        foreach ($userc->getUserWorkingGroups() as $wg) {
          if ($this->workingGroupId == $wg->getId()) {
            return TRUE;
          }
        }
      }

      return FALSE;
    }
    else {
      if ($action == MouselineAction::WRITE) {

        // following conditions must be met to edit/write the mouseline:
        // - user must be logged in
        // - user must have mouseline registrar permission
        // - user must be a member of the research group owning this mouseline

        if (!user_is_anonymous() && user_access(SFB_MOUSELINE_PERMISSION_REGISTRAR)
          && $this->userHasPermissionTo(MouselineAction::READ)) {

          $userc = UsersRepository::findByUid($user->uid);

          foreach ($userc->getUserWorkingGroups() as $wg) {
            if ($this->workingGroupId == $wg->getId()) {
              return TRUE;
            }
          }

          return FALSE;
        }
        return FALSE;

      }
      else {
        if ($action == MouselineAction::EXTEND) {
          return $this->userHasPermissionTo(MouselineAction::READ);
        }
      }
    }


    return FALSE;
  }

  //-------------------------static functions---------------------------//

  /**
   * returns the id of the mouse with given PID
   *
   * @param $pid
   *
   * @return mixed
   */
  public static function extractIDFromPID($pid) {

    if (empty($pid)) {
      return NULL;
    }

    //$regex = variable_get(SFB_MOUSELINE_CONFIG_PID_REGEX, '');
    $regex = '^(?:umg-mlc-)(mouse|mouseline)(?:-)(\d{4})$^';
    if (empty($regex)) {
      return $pid;
    }

    // Parse the PID by using regular expression.
    $match = [];
    if (preg_match($regex, $pid, $match)) {

      $id_index = variable_get(SFB_MOUSELINE_CONFIG_PID_REGEX_ID_INDEX, 0);

      foreach (explode(',', $id_index) as $index) {
        if (count($match) == $index + 1 && isset($match[$index])) {
          if ($match[$index] > 0) {
            return $match[$index] * 1;
          }
        }
      }

    }
    else {
      return NULL;
    }

    return NULL;
  }
}