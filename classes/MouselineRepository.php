<?php

/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 06.01.2017
 * Time: 15:15
 */

class MouselineRepository
{
  /**
   * Name of the table containing mouseline information.
   *
   * @var string
   */
  static $tableName = 'mouselines_mouseline';

  /**
   * Array with database fields.
   *
   * @var array
   */
  static $databaseFields = array(
    'id',
    'standard_name',
    'mpdurl',
    'strain',
    'mother_line_id',
    'father_line_id',
    'type',
    'host_name',
    'host_abbreviation',
    'donor_name',
    'donor_abbreviation',
    'mutation',
    'mutated_gene',
    'mutated_gene_symbol',
    'mutated_gene_details',
    'ncbi_gene_url',
    'lab_id',
    'workinggroup_id',
    'created_date',
    'last_change_date',
    'comments',
    'sharing_level'
  );

  /**
   * Stores mouseline data into database.
   *
   * @param Mouseline $mouseline
   * @return \DatabaseStatementInterface|int
   */
  public static function save($mouseline)
  {
    if($mouseline->isEmpty()) {
      $nid = db_insert(self::$tableName)
        ->fields(array(
          //'id' => $mouseline->getId(),
          'standard_name' => $mouseline->getName(),
          'mpdurl' => $mouseline->getMpdURL(),
          'strain' => $mouseline->getStrainId(),
          'mother_line_id' => $mouseline->getMotherLineId(),
          'father_line_id' => $mouseline->getFatherLineId(),
          'type' => $mouseline->getType(),
          'host_name' => $mouseline->getHostName(),
          'host_abbreviation' => $mouseline->getHostAbbr(),
          'donor_name' => $mouseline->getDonorName(),
          'donor_abbreviation' => $mouseline->getDonorAbbr(),
          'mutation' => $mouseline->getMutationType(),
          'mutated_gene' => $mouseline->getMutatedGene(),
          'mutated_gene_symbol' => $mouseline->getMutatedGeneSymbol(),
          'mutated_gene_details' => $mouseline->getMutatedGeneDetails(),
          'ncbi_gene_url' => $mouseline->getNcbiGeneURL(),
          'lab_id' => $mouseline->getLaboratoryId(),
          'workinggroup_id' => $mouseline->getWorkingGroupId(),
          'created_date' => $mouseline->getCreatedDate(),
          'last_change_date' => date('Y-m-d'),
          'comments' => $mouseline->getComments(),
          'sharing_level' => $mouseline->getSharingLevel(),

        ))
          ->execute();
      return $nid;
    } else {
      $num_updated = db_update(self::$tableName)
        ->fields(array(
          //'id' => $mouseline->getId(),
          'standard_name' => $mouseline->getName(),
          'mpdurl' => $mouseline->getMpdURL(),
          'strain' => $mouseline->getStrainId(),
          'mother_line_id' => $mouseline->getMotherLineId(),
          'father_line_id' => $mouseline->getFatherLineId(),
          'type' => $mouseline->getType(),
          'host_name' => $mouseline->getHostName(),
          'host_abbreviation' => $mouseline->getHostAbbr(),
          'donor_name' => $mouseline->getDonorName(),
          'donor_abbreviation' => $mouseline->getDonorAbbr(),
          'mutation' => $mouseline->getMutationType(),
          'mutated_gene' => $mouseline->getMutatedGene(),
          'mutated_gene_symbol' => $mouseline->getMutatedGeneSymbol(),
          'mutated_gene_details' => $mouseline->getMutatedGeneDetails(),
          'ncbi_gene_url' => $mouseline->getNcbiGeneURL(),
          'lab_id' => $mouseline->getLaboratoryId(),
          'workinggroup_id' => $mouseline->getWorkingGroupId(),
          'created_date' => $mouseline->getCreatedDate(),
          'last_change_date' => date('Y-m-d'),
          'comments' => $mouseline->getComments(),
          'sharing_level' => $mouseline->getSharingLevel(),

        ))
        ->condition('id',$mouseline->getId(), '=')
        ->execute();
      return -1;
    }
  }

  /**
   * transforms query results into mouse line objects
   *
   * @param $result
   * @return Mouseline
   */
  public static function databaseResultsToMouseline($result) {
    $mouseline = new Mouseline();

    if(empty($result)) {
      return $mouseline;
    }

    // set variables
    $mouseline->setId($result->id);
    $mouseline->setName($result->standard_name);
    $mouseline->setMpdURL($result->mpdurl);
    $mouseline->setStrainId($result->strain);
    $mouseline->setMotherLineId($result->mother_line_id);
    $mouseline->setFatherLineId($result->father_line_id);
    $mouseline->setType($result->type);
    $mouseline->setHostName($result->host_name);
    $mouseline->setHostAbbr($result->host_abbreviation);
    $mouseline->setDonorName($result->donor_name);
    $mouseline->setDonorAbbr($result->donor_abbreviation);
    $mouseline->setMutationType($result->mutation);
    $mouseline->setMutatedGene($result->mutated_gene);
    $mouseline->setMutatedGeneSymbol($result->mutated_gene_symbol);
    $mouseline->setMutatedGeneDetails($result->mutated_gene_details);
    $mouseline->setNcbiGeneURL($result->ncbi_gene_url);
    $mouseline->setLaboratoryId($result->lab_id);
    $mouseline->setWorkingGroupId($result->workinggroup_id);
    $mouseline->setCreatedDate($result->created_date);
    $mouseline->setLastModified($result->last_change_date);
    $mouseline->setComments($result->comments);
    $mouseline->setSharingLevel($result->sharing_level);
    return $mouseline;
  }

  /**
   * Reads mouseline database results and create an array with Mouseline objects.
   *
   * @param $results
   * @return \Mouseline[]
   */
  public static function databaseResultsToMouselines($results) {
    $mouselines = array();
    foreach($results as $result)
      $mouselines[] = self::databaseResultsToMouseline($result);

    return $mouselines;
  }

  /**
   * gives the newest entries to the database. $limit defines the amount of
   * entries that are returned
   *
   * @param $limit
   * @return Mouseline[]
   */


  /**
   * Return IDs of all Mouselines.
   *
   * @return array IDs found
   */
  public static function getAllIds(){
    $results = db_select(self::$tableName,'id')
      ->fields('id',['id'])
      ->execute()
      ->fetchCol();
    return $results;
  }
  public static function findAndSortByCreatedDateLimit($limit) {
    // sharing level condition
    $sl_condition = MouselineSharingLevel::getDatabaseCondition();

    $results = db_select(self::$tableName, 'm')
      ->condition($sl_condition)
      ->fields('m', self::$databaseFields)
      ->range(0,$limit)
      ->orderBy('created_date')
      ->execute();

    return self::databaseResultsToMouselines($results);
  }

  /**
   * finds mouse line by id.
   *
   * @param $id
   * @return Mouseline
   */
  public static function findById($id)
  {
    $result = db_select(self::$tableName, 'm')
      ->condition('id', $id, '=')
      ->fields('m', self::$databaseFields)
      ->range(0, 1)
      ->execute()
      ->fetch();

    return self::databaseResultsToMouseline($result);
  }

  /**
   * Returns all mouselines with ordering and pager default.
   *
   * @param $header
   * @return \Mouseline[]
   */
  public static function findByTypeUseTableSortAndUsePagerDefault($header, $limit = 5) {

    // sharing level condition
    $sl_condition = MouselineSharingLevel::getDatabaseCondition();

    $results = db_select(self::$tableName, 'a')
      ->extend('TableSort')
      ->fields('a', self::$databaseFields)
      ->condition($sl_condition)
      ->extend('PagerDefault')
      ->limit($limit)
      ->orderByHeader($header)
      ->orderBy('id')
      ->execute()
    ;
    return self::databaseResultsToMouselines($results);
  }

  /**
   * count all mouselines
   *
   * @return int
   */
  public static function getMouselinesNumber() {
    $results = db_select(self::$tableName, 'a')
      ->fields('a', self::$databaseFields)
      ->execute();
    return $results->rowCount();
  }

  /**
   * Executes search query on Mouseline database with given condition.
   *
   * @param $condition Database condition
   * @return \Mouseline[]
   */
  public static function findBy($condition) {
    // sharing level condition
    $sl_condition = MouselineSharingLevel::getDatabaseCondition();

    $results = db_select(self::$tableName, 'm')
      ->condition($condition)
      ->condition($sl_condition)
      ->fields('m', self::$databaseFields)
      ->execute();

    return self::databaseResultsToMouselines($results);
  }


  /**
   * Quick and dirty way to look up all linked publications...
   *
   * @param $id
   * @return mixed
   */
  public static function lookUpPubs($id){
    $pubFields = array(
        'mouselines_mouseline',
        'pubreg_articles',
    );

    $results = db_select('rdp_linking_mouselines_mouseline_pubreg_articles', 'r')
        ->condition('mouselines_mouseline', $id, '=')
        ->fields('r', $pubFields)
        ->range(0, 1)
        ->execute()
        ->fetch();
    return $results;
  }
}
