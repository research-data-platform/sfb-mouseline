<?php

/**
 * Created by PhpStorm.
 * User: Paul_Roman
 * Date: 06.01.2017
 * Time: 15:15
 */
class Mouseline {

  /* ------------------------------ Attributes ----------------------------- */
  /**
   * Default id for new or yet unsaved Mouse line objects.
   *
   * @var int
   */
  const EMPTY_MOUSELINE_ID = -1;


  /**
   * Date when the mouse line was entered to the module
   * Format: yyyy-mm-dd
   *
   * @var string
   */
  private $createdDate;

  /**
   * Date when the mouse line was edited the last time
   *
   * @var string
   */
  private $lastModified;

  /**
   * @var int
   */
  private $id = Mouseline::EMPTY_MOUSELINE_ID;

  /**
   * Type of Mouse line breeding (e.g. Targeted Mutation, Hybrid, etc.)
   *
   * @see MouselineType
   * @var string
   */
  private $type;

  /**
   * research group's id
   *
   * @var int
   */
  private $workingGroupId;

  /**
   * standardised name after nomenclature of the Committee for Standardized
   * Genetic Nomenclature in Mice value finally goes into db field
   * "standard_name"
   *
   * @var string
   */
  private $name;

  /**
   * @var string
   */
  private $description;

  /**
   * origin laboratory's id
   *
   * @see MouselineLaboratory
   * @var int
   */
  private $laboratoryId;

  /**
   * for inbred lines: origin strain's id
   *
   * @see MouselineStrain
   * @var int
   */
  private $strainId;

  /**
   * custom comments
   *
   * @var string
   */
  private $comments;

  /**
   * Level of visibility for the mouse line, values:
   * 10 = private, currently not used
   * 20 = (research) group wide
   * 30 = site/rdp wide
   * 90 = public
   *
   * @see MouselineSharingLevel
   * @var int
   */
  private $sharing_level;

  /*Attributes for Hybrid mouselines*/
  /**
   * for hybrid lines: mother's mouse lines's id
   *
   * @var int
   */
  private $mother_line_id;

  /**
   * for hybrid lines: father's mouse lines's id
   *
   * @var int
   */
  private $father_line_id;

  /**
   * for mixed inbred lines: host strain name
   *
   * @var string
   */
  private $hostName;

  /**
   * for mixed inbred lines: host strain abbreviation
   *
   * @var string
   */
  private $hostAbbr;

  /**
   * for mixed inbred lines: donor strain name
   *
   * @var string
   */
  private $donorName;

  /**
   * for mixed inbred lines: host strain abbreviation
   *
   * @var string
   */
  private $donorAbbr;

  /**
   * e.g. targeted mutation, spontaneous, etc.
   *
   * @var string
   */
  private $mutationType;

  /**
   * target of the genetic alteration
   *
   * @var string
   */
  private $mutatedGene;

  /**
   * target of the genetic alteration (official symbol)
   *
   * @var string
   */
  private $mutatedGeneSymbol;

    /**
     * target of the genetic alteration (official symbol)
     *
     * @var string
     */
  private $mutatedGeneDetails;

  /**
   * URL of gene in https://www.ncbi.nlm.nih.gov/gene
   *
   * @var string
   */
  private $ncbiGeneURL;

  /**
   * line's Mouse Phenome Database URL
   *
   * @var string
   */
  private $mpdURL;

  /* ------------------------------ Setters & Getters ----------------------------- */

  /**
   * @return string
   */
  public function getCreatedDate() {
    return $this->createdDate;
  }

  /**
   * @param string $createdDate
   */
  public function setCreatedDate($createdDate) {
    $this->createdDate = $createdDate;
  }

  /**
   * @return string
   */
  public function getLastModified() {
    return $this->lastModified;
  }

  /**
   * @param string $lastModified
   */
  public function setLastModified($lastModified) {
    $this->lastModified = $lastModified;
  }

  /**
   * @return int
   */
  public function getId() {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId($id) {
    $this->id = $id;
  }


  /**
   * @return string
   */
  public function getType() {
    return $this->type;
  }

  /**
   * @param string $type
   */
  public function setType($type) {
    $this->type = $type;
  }

  /**
   * @return int
   */
  public function getWorkingGroupId() {
    return $this->workingGroupId;
  }

  /**
   * @param int $workingGroupId
   */
  public function setWorkingGroupId($workingGroupId) {
    $this->workingGroupId = $workingGroupId;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName($name) {
    $this->name = $name;
  }

  /**
   * @return string
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * @param string $description
   */
  public function setDescription($description) {
    $this->description = $description;
  }

  /**
   * @return int
   */
  public function getStrainId() {
    return $this->strainId;
  }

  /**
   * @param int $strainId
   */
  public function setStrainId($strainId) {
    $this->strainId = $strainId;
  }

  /**
   * @return int
   */
  public function getLaboratoryId() {
    return $this->laboratoryId;
  }

  /**
   * @param int $laboratoryId
   */
  public function setLaboratoryId($laboratoryId) {
    $this->laboratoryId = $laboratoryId;
  }

  /**
   * @return string
   */
  public function getComments() {
    return $this->comments;
  }

  /**
   * @param string $comments
   */
  public function setComments($comments) {
    $this->comments = $comments;
  }

  /**
   * @return int
   */
  public function getSharingLevel() {
    return $this->sharing_level;
  }

  /**
   * @param int $sharing_level
   */
  public function setSharingLevel($sharing_level) {
    $this->sharing_level = $sharing_level;
  }

  /**
   * looks up the mouse line's working/research group in the working group
   * repository
   *
   * @return WorkingGroup
   * @see WorkingGroup
   */
  public function getWorkingGroup() {
    return WorkingGroupRepository::findById($this->workingGroupId);
  }

  /**
   * @return string
   */
  public function getHostName() {
    return $this->hostName;
  }

  /**
   * @param string $hostName
   */
  public function setHostName($hostName) {
    $this->hostName = $hostName;
  }

  /**
   * @return string
   */
  public function getHostAbbr() {
    return $this->hostAbbr;
  }

  /**
   * @param string $hostAbbr
   */
  public function setHostAbbr($hostAbbr) {
    $this->hostAbbr = $hostAbbr;
  }

  /**
   * @return string
   */
  public function getDonorName() {
    return $this->donorName;
  }

  /**
   * @param string $donorName
   */
  public function setDonorName($donorName) {
    $this->donorName = $donorName;
  }

  /**
   * @return string
   */
  public function getDonorAbbr() {
    return $this->donorAbbr;
  }

  /**
   * @param string $donorAbbr
   */
  public function setDonorAbbr($donorAbbr) {
    $this->donorAbbr = $donorAbbr;
  }

  /**
   * @return string
   */
  public function getMutationType() {
    return $this->mutationType;
  }

  /**
   * @param string $mutationType
   */
  public function setMutationType($mutationType) {
    $this->mutationType = $mutationType;
  }

  /**
   * @return string
   */
  public function getMutatedGene() {
    return $this->mutatedGene;
  }

  /**
   * @param string $mutatedGene
   */
  public function setMutatedGene($mutatedGene) {
    $this->mutatedGene = $mutatedGene;
  }

  /*Attribute-setters/getters for Hybrid mouselines*/
  /**
   * @return int
   */
  public function getMotherLineId() {
    return $this->mother_line_id;
  }

  /**
   * @param int $mother_line_id
   */
  public function setMotherLineId($mother_line_id) {
    $this->mother_line_id = $mother_line_id;
  }

  /**
   * @return int
   */
  public function getFatherLineId() {
    return $this->father_line_id;
  }

  /**
   * @param int $father_line_id
   */
  public function setFatherLineId($father_line_id) {
    $this->father_line_id = $father_line_id;
  }

  /**
   * @return string
   */
  public function getMpdURL() {
    return $this->mpdURL;
  }

  /**
   * @param string $mpdURL
   */
  public function setMpdURL($mpdURL) {
    $this->mpdURL = $mpdURL;
  }

  /**
   * @return string
   */
  public function getMutatedGeneSymbol() {
    return $this->mutatedGeneSymbol;
  }

  /**
   * @param string $mutatedGeneSymbol
   */
  public function setMutatedGeneSymbol($mutatedGeneSymbol) {
    $this->mutatedGeneSymbol = $mutatedGeneSymbol;
  }

    /**
     * @return string
     */
    public function getMutatedGeneDetails()
    {
        return $this->mutatedGeneDetails;
    }

    /**
     * @param string $mutatedGeneSymbol
     */
    public function setMutatedGeneDetails($mutatedGeneDetails)
    {
        $this->mutatedGeneDetails = $mutatedGeneDetails;
    }

  /**
   * @return string
   */
  public function getNcbiGeneURL() {
    return $this->ncbiGeneURL;
  }

  /**
   * @param string $ncbiGeneURL
   */
  public function setNcbiGeneURL($ncbiGeneURL) {
    $this->ncbiGeneURL = $ncbiGeneURL;
  }

  /* ------------------------------ Form fields ----------------------------- */

  /**
   * returns form field for the mouse line's name. also contains the ajax
   * target div for the mouse line's name generation
   *
   * @return array
   */
  public function getFormFieldName() {
    return [
      '#type' => 'textfield',
      '#title' => t('Name') . "<small> The mouseline's name will be dynamically generated while you input its data.</small>",
      '#description' => 'The mouseline\'s name will be dynamically generated while you input its data.',
      '#default_value' => $this->name,
      '#disabled' => TRUE,
      '#prefix' => '<div id="mouseline-name-div">',
      '#suffix' => '</div>',
    ];
  }

  /**
   * This formfield is used for searching for mouselines in the database via
   * AJAX, e.g. for hybrids, currently options may contain whether the user
   * wishes to look up line id's for the father or the mother Also builds the
   * name of the respective ajax callback which is triggered when user inputs
   * data
   *
   * @param string $option
   *
   * @return array
   */
  public function getFormFieldNameSearch($option = "") {
    $default = '';
    if ($option != "") {
      $parent = "";
      if ($option == 'mother') {
        $parent = MouselineRepository::findById($this->getMotherLineId());
      }
      if ($option == 'father') {
        $parent = MouselineRepository::findById($this->getFatherLineId());
      }
      $option .= "_";
      if (!empty($parent->getName())) {
        $default = $parent->getName() . ", PID: " . $parent->getElementPID();
      }
    }
    return [
      '#type' => 'textfield',
      '#title' => t('Enter mouseline abbreviation'),
      '#description' => 'Here you can search for a specific mouseline. An autocomplete function will assist you after you entered the first letters',
      '#attributes' => ['class' => ['mouseline']],
      '#default_value' => $default,

      '#multiple' => 'true',
      '#autocomplete_path' => SFB_MOUSELINE_API_AUTOCOMPLETE_INBRED,
      '#ajax' => [
        'callback' => 'ajax_mouseline_' . $option . 'details_callback',
        'wrapper' => 'mouseline_' . $option . 'details_div',
      ],
    ];
  }

  /**
   * returns form field for the Mouse Phenome DB URL
   *
   * @return array
   */
  public function getFormfieldMPDURL() {
    return [
      '#type' => 'textfield',
      '#title' => t('Mouse Phenome Database URL'),
      '#default_value' => $this->mpdURL,
      '#description' => 'Insert the mouseline\'s <a href="http://phenome.jax.org/strains" target="_blank">Mouse Phenome Database</a></small> URL here. (if available)',
      '#attributes' => ['placeholder' => t('e.g.') . ': http://phenome.jax.org/strains/7'],
      '#maxlength' => 250,
    ];
  }

  /**
   * returns the select field for add Data
   *
   * @return array
   */
  public function getFormfieldDataType() {
    return [
      '#type' => 'select',
      '#title' => t('Data'),
      '#required' => TRUE,
      '#options' => [
        "Antibody" => "Antibody",
        //TODO
      ],
      '#description' => t('Select the Type of data you want to link.'),
      '#ajax' => [
        'callback' => 'ajax_data_selection_callback',
        'wrapper' => 'mouseline-name-div',
      ],
    ];
  }

  /**
   * returns the select field for Congenic strains to test whether mutated chromosome is known
   *
   * @return array
   */
  public function getFormfieldCongenicComplex() {
    $form['complex_options'] = [
      '#type' => 'value',
      '#value' => ['NO' => t('no'),'YES' => t('yes')]
    ];
    return [
      '#type' => 'select',
      '#title' => t('Is the chromosome on which the mutation arose unknown? (i.e. the genetic origin is complex)'),
      '#options' => $form['complex_options']['#value'],
      '#description' => t('Select \'yes\' if the donor is not inbred, is complex or an F1 hybrid. Select \'yes\' if multiple alleles in the donor strain
        came from more than one source or if the genetic origin of at leat one allele in the strain name is uncertain. Select \'yes\' if the donor strain was
        constructed by crossing two congenic strains with different respective donor strains. Select \'no\' otherwise.'),
      '#ajax' => [
        'callback' => 'ajax_mouseline_name_callback',
        'wrapper' => 'mouseline-name-div',
      ],
    ];
  }

  /**
   * returns the text field for host strain
   *
   * @return array
   */
  public function getFormfieldHostName() {
    return [
      '#type' => 'textfield',
      '#title' => t('Host strain'),
      '#description' => t('Insert complete name of host strain.'),
      '#default_value' => $this->getHostName(),
      '#attributes' => ['placeholder' => t('e.g.') . ': C57BL/6'],
      '#maxlength' => 250,

    ];
  }
  /**
   * returns the text field for host strain abbreviation
   *
   * @return array
   */
  public function getFormfieldHostAbbr() {
    return [
      '#type' => 'textfield',
      '#title' => t('Host strain abbreviation'),
      '#description' => t('Insert the abbreviation of the host strain.'),
      '#default_value' => $this->getHostAbbr(),
      '#attributes' => ['placeholder' => t('e.g.') . ': B6'],
      '#maxlength' => 250,

    ];
  }
  /**
   * returns the text field for donor strain
   *
   * @return array
   */
  public function getFormfieldDonorName() {
    return [
      '#type' => 'textfield',
      '#title' => t('Donor strain'),
      '#description' => t('Insert complete name of donor strain.'),
      '#default_value' => $this->getDonorName(),
      '#attributes' => ['placeholder' => t('e.g.') . ': DBA/2N'],
      '#maxlength' => 250,

    ];
  }
  /**
   * returns the text field for donor strain abbreviation
   *
   * @return array
   */
  public function getFormfieldDonorAbbr() {
    return [
      '#type' => 'textfield',
      '#title' => t('Donor strain abbreviation'),
      '#description' => t('Insert the abbreviation of the donor strain.'),
      '#default_value' => $this->getDonorAbbr(),
      '#attributes' => ['placeholder' => t('e.g.') . ': D2'],
      '#maxlength' => 250,

    ];
  }
  /**
   * returns the select field for the mouseline's mutation type
   *
   * @return array
   */
  public function getFormfieldMutation() {
    return [
      '#type' => 'select',
      '#title' => t('Mutation Type'),
      '#default_value' => $this->getMutationType(),
      '#options' => [
        MouselineMutation::WILDTYPE => MouselineMutation::WILDTYPE,
      //  MouselineMutation::TARGETED_MUTATION => MouselineMutation::TARGETED_MUTATION,
        MouselineMutation::OTHER_MUTATION => MouselineMutation::OTHER_MUTATION,
        //TODO
      ],
      '#description' => t('Select the Type of Mutation.'),
      '#ajax' => [
        'callback' => 'ajax_mouseline_name_callback',
        'wrapper' => 'mouseline-name-div',
      ],
    ];
  }

  /**
   * returns the text field for mutated gene
   *
   * @return array
   */
  public function getFormfieldMutatedGene() {
    return [
      '#type' => 'textfield',
      '#title' => t('Gene'),
      '#description' => t('Insert affected gene.'),
      '#default_value' => $this->getMutatedGene(),
      '#attributes' => ['placeholder' => t('e.g.') . ': Neurogenin 3'],
      '#maxlength' => 250,

    ];
  }

  /**
   * returns the text field for mutated gene (symbol)
   *
   * @return array
   */
  public function getFormfieldMutatedgeneSymbol() {
    return [
      '#type' => 'textfield',
      '#title' => t('Official gene symbol <small>(can be found in the <a href="https://www.ncbi.nlm.nih.gov/gene" target="_blank">NCBI Gene DB</a>)</small>'),
      '#description' => t('Insert gene symbol.'),
      '#default_value' => $this->getMutatedGeneSymbol(),
      '#attributes' => ['placeholder' => t('e.g.') . ': Neurog3'],
      '#maxlength' => 250,
      '#ajax' => [
        'callback' => 'ajax_mouseline_name_callback',
        'wrapper' => 'mouseline-name-div',
      ],
    ];
  }

    /**
     * returns the text field with details on the mutated gene
     *
     * @return array
     */
    public function getFormfieldMutatedGeneDetails()
    {
        return array(
            '#type' => 'textfield',
            '#title' => t('Details on the mutated gene <small>(e.g. mutation type, allele number, creator lab code)</small>'),
            '#description' => t('Insert details on the mutated gene.'),
            '#description' => 'The details on the mutated gene will be superscripted to the gene symbol (e.g. Neurog3<sup>tm1Mom</sup>). </small> ',
            '#default_value' => $this->getMutatedGeneDetails(),
            '#attributes' => array('placeholder' => t('e.g.') . ': tm1Mom'),
            '#maxlength' => 250,
            '#ajax' => array(
                'callback' => 'ajax_mouseline_name_callback',
                'wrapper' => 'mouseline-name-div',
            )
        );
    }

  /**
   * returns the text field for the mutated gene's URL on ncbi genes
   *
   * @return array
   */
  public function getFormfieldMutatedgeneNcbiURL() {
    return [
      '#type' => 'textfield',
      '#title' => t('Gene\'s URL in NCBI Gene DB'),
      '#description' => t('Insert URL.'),
      '#default_value' => $this->getNcbiGeneURL(),
      '#attributes' => ['placeholder' => t('e.g.') . ': https://www.ncbi.nlm.nih.gov/gene/11925'],
      '#maxlength' => 250,
    ];
  }

  /**
   * returns form field for comment
   *
   * @return array
   */
  public function getFormFieldComments() {
    return [
      '#type' => 'textarea',
      '#title' => t('Comments'),
      '#default_value' => $this->comments,
      '#attributes' => ['placeholder' => t('e.g.') . ' important information'],
      '#description' => 'Here you can insert information you want others to know.',
      '#maxlength' => 100,
    ];
  }

  /**
   * returns drop down to select working group
   *
   * @return array
   */
  public function getFormFieldWorkingGroup() {
    global $user;

    $userc = UsersRepository::findByUid($user->uid);
    $workingGroups = $userc->getUserWorkingGroups();

    $workingGroupsSelect = [];
    foreach ($workingGroups as $workingGroup) {
      $workingGroupsSelect[$workingGroup->getId()] = $workingGroup->getName();
    }

    $default_value = $this->workingGroupId;

    //TODO: fall einbauen, dass der benutzer zu keiner arbeitsgruppe gehört, evtl dann sharing level private

    return [
      '#type' => 'select',
      '#title' => t('Research Group'),
      '#required' => TRUE,
      '#options' => $workingGroupsSelect,
      '#default_value' => $default_value,
      '#description' => t('Select research group owning this mouseline.'),
    ];
  }

  /**
   * returns drop down for selecting the mouse line's type
   *
   * @return array
   */
  public function getFormFieldType() {
    //if no type is set yet
    if (empty($this->getType())) {
      $types = [];
      //fetch all available types
      $type_options = MouselineType::getAllTypes();
      foreach ($type_options as $type) {
        $types[$type] = $type;
      }

      return [
        '#type' => 'select',
        '#title' => t('Type'),
        '#description' => 'Select the mouseline\'s type.',
        '#options' => $types,
      ];
    }
    elseif ($this->getType() == MouselineType::UNSPECIFIED) {
      $types = [];
      //fetch all available types
      $type_options = MouselineType::getAllTypes();
      foreach ($type_options as $type) {
        $types[$type] = $type;
      }

      return [
        '#type' => 'select',
        '#title' => t('Type'),
        '#default_value' => $this->getType(),
        '#description' => 'Select the mouseline\'s type.',
        '#options' => $types,
      ];
    }
    else {
      return [
        '#type' => 'textfield',
        '#title' => t('Type'),
        '#default_value' => $this->getType(),
        '#maxlength' => 100,
        '#disabled' => TRUE,
      ];
    }
  }

  /**
   * adds a dropdown to select the visibility pf the mouse line
   *
   * @return array
   * @see MouselineSharingLevel
   */
  public function getFormFieldSharingLevel() {
    $allSharingLevels = MouselineSharingLevel::getAsArray();

    if (empty($this->sharingLevel)) {
      $this->sharingLevel = variable_get(SFB_MOUSELINE_CONFIG_DEFAULT_SHARING_LEVEL,
        MouselineSharingLevel::GROUP_LEVEL);
    }

    return [
      '#type' => 'select',
      '#title' => t('Sharing level'),
      '#description' => 'Change the visibility (sharing) level of this mouseline.',
      '#suffix' => '
        <small>
        <strong>Group:</strong> Only members of your research group can see the current mouseline.<br />
        <strong>Site:</strong> All users of the Research Data Platform are allowed to see the current mouseline.<br />
        <strong>Public:</strong> Everyone (also anonymous user) is allowed to see the current mouseline.<br />
        </small>',
      '#default_value' => $this->sharingLevel,
      '#required' => TRUE,
      '#options' => $allSharingLevels,
    ];
  }

  /* ------------------------------ Field Views ----------------------------- */
  /**
   * inserts the working group's icon, embedded in HTML code
   *
   * @return string
   */
  public function getElementWorkingGroupIcon() {
    $workingGroup = $this->getWorkingGroup();
    return '<img src="' . $workingGroup->getIconPath() . '"
      data-toggle="tooltip" title="' . $workingGroup->getName() . '" />';
  }

  public function getElementId() {
    return str_pad($this->id, 4, '0', STR_PAD_LEFT);
  }

  /**
   * takes the mouse line's id and returns the PID with pattern
   * 'mouseline-{00ID}'
   *
   * @return mixed
   */
  public function getElementPID() {
    $pid_id = $this->id;
    // if padding > 0, then pad a id string to given length
    if (variable_get(SFB_MOUSELINE_CONFIG_PID_ID_PADDING, 4) > 0) {
      $pid_id = str_pad($pid_id,
        variable_get(SFB_MOUSELINE_CONFIG_PID_ID_PADDING, 4), 0, STR_PAD_LEFT);
    }
    $pattern = variable_get(SFB_MOUSELINE_CONFIG_PID_PATTERN, '');

    $replaced = preg_replace(['/{type}/', '/{pid}/'], ['mouseline', $pid_id],
      $pattern);

    return $replaced;
  }

  /**
   * Returns html formatted string with the name of the sharing level.
   *
   * @return string
   */
  public function getElementSharingLevel() {
    $level = 'Unknown';

    switch ($this->sharing_level) {
      case MouselineSharingLevel::PRIVATE_LEVEL:
        $level = 'Private';
        break;
      case MouselineSharingLevel::GROUP_LEVEL:
        $level = 'Group';
        break;
      case MouselineSharingLevel::SITE_LEVEL:
        $level = 'Site';
        break;
      case MouselineSharingLevel::PUBLIC_LEVEL:
        $level = 'Public';
        break;
    }

    return $level;
  }

  /**
   * returns all publications in which this mouse line is used
   *
   * @return array
   */
  /*public function getLinkedPublications(){
    $pubs = MouselineRepository::lookUpPubs($this->id);
    $user_wgs = getWorkinggroupsOfCurrentUser();
    foreach ($pubs as $pub){
      $articles[] = fetchPubAndCheck($pub->id, $user_wgs, false);
    }
    if (empty($articles)){
      $articles = "none";
    }
    return $articles;
  }*/

  public function getLinkedPublications() {
    $rdp_linking = RDPLinking::getLinkingByName('sfb_literature',
      'pubreg_articles');
    if ($rdp_linking != NULL) {
      $data = $rdp_linking->getViewData('mouselines_mouseline', $this->id);
      return $data;
    }

    return ['table' => ['rows' => [], 'header' => []]];
  }

  /**
   * saves the mouse line, calls database function
   *
   * @return DatabaseStatementInterface|int
   */
  public function save() {
    $nid = MouselineRepository::save($this);
    return $nid;
  }

  /**
   * Checks whether given mouseline is new or already in DB
   *
   * @return bool
   */
  public function isEmpty() {
    if ($this->id == Mouseline::EMPTY_MOUSELINE_ID) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * adds a div which contains a short overview of the mouse line
   *
   * @return string
   */
  public function getInformationField() {
    $string = "";
    if (!empty($this->getName())) {
      $string = '<div>
                <strong>PID: </strong>' . $this->getElementPID() . '<br>
                <strong>Name: </strong>' . $this->getName() . '<br>
                <strong>Created: </strong>' . $this->getCreatedDate() . '<br>
                <strong>Comments: </strong>' . $this->getComments() . '<br>
                </div>';
    }
    else {
      $string = '<div>
            <i>Strain or mouse line not found in Database.</i>
        ';
    }
    return $string;
  }

  /**
   * checks whether the user is allowes to {MouselineAction} given line
   *
   * @param MouselineAction $action
   *
   * @return bool
   * @see MouselineSharingLevel
   * @see MouselineAction
   *
   */
  public function userHasPermissionTo($action) {
    global $user;
    $userc = UsersRepository::findByUid($user->uid);

    if ($action == MouselineAction::READ) {

      if ($this->getSharingLevel() == MouselineSharingLevel::PUBLIC_LEVEL) {
        return TRUE;
      }

      if (user_is_anonymous()) {
        return FALSE;
      }

      if ($this->getSharingLevel() == MouselineSharingLevel::SITE_LEVEL) {
        return TRUE;
      }

      $userc = UsersRepository::findByUid($user->uid);

      if ($userc->isEmpty()) {
        return FALSE;
      }

      if ($this->getSharingLevel() == MouselineSharingLevel::GROUP_LEVEL) {
        foreach ($userc->getUserWorkingGroups() as $wg) {
          if ($this->workingGroupId == $wg->getId()) {
            return TRUE;
          }
        }
      }

      return FALSE;
    }
    else {
      if ($action == MouselineAction::WRITE) {

        // following conditions must be met to edit/write the mouseline:
        // - user must be logged in
        // - user must have mouseline registrar permission
        // - user must be a member of the research group owning this mouseline

        if (!user_is_anonymous() && user_access(SFB_MOUSELINE_PERMISSION_REGISTRAR)
          && $this->userHasPermissionTo(MouselineAction::READ)) {

          $userc = UsersRepository::findByUid($user->uid);

          foreach ($userc->getUserWorkingGroups() as $wg) {
            if ($this->workingGroupId == $wg->getId()) {
              return TRUE;
            }
          }

          return FALSE;
        }
        return FALSE;

      }
      else {
        if ($action == MouselineAction::EXTEND) {
          return $this->userHasPermissionTo(MouselineAction::READ);
        }
      }
    }


    return FALSE;
  }

  /* -------------------------- Static functions ---------------------------- */

  /**
   * returns the id of the mouse line with given PID
   *
   * @param $pid
   *
   * @return mixed
   */
  public static function extractIDFromPID($pid) {
    if (empty($pid)) {
      return NULL;
    }

    $regex = '^(?:umg-mlc-)(mouse|mouseline)(?:-)(\d{4})$^';
    if (empty($regex)) {
      return $pid;
    }

    //
    // Parse the PID by using regular expression.
    //

    $match = [];
    if (preg_match($regex, $pid, $match)) {

      $id_index = variable_get(SFB_MOUSELINE_CONFIG_PID_REGEX_ID_INDEX, 0);

      //
      // Since the return value of preg_match function ($match) can contain
      // more than one return values, the administrator can set more than
      // one value for getting an ID.
      // This is useful if you plan to support more than one antibody PID pattern.
      //
      foreach (explode(',', $id_index) as $index) {
        if (count($match) == $index + 1 && isset($match[$index])) {
          if ($match[$index] > 0) {
            return $match[$index] * 1;
          }
        }
      }

    }
    else {
      return NULL;
    }

    return NULL;

  }
}

/**
 * Class MouselineSharingLevel
 * regulates the visibility of data in the MLC module
 */
class MouselineSharingLevel {

  const PRIVATE_LEVEL = 10;

  const GROUP_LEVEL = 20;

  const SITE_LEVEL = 30;

  const PUBLIC_LEVEL = 90;

  public static function getAsArray() {
    return [
      #10 => 'Private',
      20 => 'Group',
      30 => 'Site',
      90 => 'Public',
    ];
  }

  /**
   * fetches the name of the given sharing level. returns it in lower case
   * if $toLower is true
   *
   * @param $sharing_level
   * @param bool $toLower
   *
   * @return string
   */
  public static function getName($sharing_level, $toLower = FALSE) {
    $sharing_level_name = '';

    switch ($sharing_level) {
      case MouselineSharingLevel::PRIVATE_LEVEL:
        $sharing_level_name = 'Private';
        break;
      case MouselineSharingLevel::GROUP_LEVEL:
        $sharing_level_name = 'Group';
        break;
      case MouselineSharingLevel::SITE_LEVEL:
        $sharing_level_name = 'Site';
        break;
      case MouselineSharingLevel::PUBLIC_LEVEL:
        $sharing_level_name = 'Public';
        break;
      default:
        $sharing_level_name = 'Unknown';
    }

    if ($toLower) {
      return strtolower($sharing_level_name);
    }

    return $sharing_level_name;
  }

  /* ------------------------------ Form fields ----------------------------- */
  /**
   * adds a dropdown to select the visibility pf the mouse line
   *
   * @return array
   */
  public static function getFormFieldSharingLevel() {
    $sharing_levels = MouselineSharingLevel::getAsArray();
    return [
      '#type' => 'select',
      '#title' => t('Sharing Level'),
      '#description' => 'Select visibility of new mouseline.',
      '#suffix' => 'Group: Only members of the creator\'s group can see the current mouseline.
      <br>Site: All users of the site are allowed to see the current mouseline. <br>Public: Everyone is allowed to see the current mouseline.',
      '#default_value' => 20,
      '#required' => TRUE,
      '#options' => $sharing_levels,
    ];
  }

  /**
   * gives database queries a visibility condition, filters search results with
   * regards to user rights
   *
   * @return mixed
   */
  public static function getDatabaseCondition() {
    $db_or = db_or();

    // public level
    $db_or->condition('sharing_level', MouselineSharingLevel::PUBLIC_LEVEL,
      '=');

    // site level
    if (user_is_anonymous()) {
      return $db_or;
    }

    $db_or->condition('sharing_level', MouselineSharingLevel::SITE_LEVEL, '=');

    // group level
    global $user;
    $userc = UsersRepository::findByUid($user->uid);
    foreach ($userc->getUserWorkingGroups() as $wg) {
      $db_and = db_and();
      $db_and->condition('sharing_level', MouselineSharingLevel::GROUP_LEVEL,
        '=');
      $db_and->condition('workinggroup_id', $wg->getId(), '=');
      $db_or->condition($db_and);
    }

    // private level
    //TODO: private level implementieren

    return $db_or;
  }

}

/**
 * Class MouselineAction
 * defines possible actions
 */
class MouselineAction {

  const READ = 'read';

  const WRITE = 'write';

  const EXTEND = 'extend';
}
